﻿Simple Spring Boot pet project to explore Spring and any other Java-based technologies. 

### Dependencies
#### Spring Boot Starters
* Web
* Data JPA
* Data REST
* Log4j2

#### Others
* Java 8
* Project Lombok
* Jackson Databind

### Current Features
#### Spring Data exploration
* Use of Spring Data JPA and in-memory H2 database  to create simple entity classes and interact with using Spring Data REST. 
* Explore different ways to test - using Mockito, MockBean, @SpringBootTest integration test, @ContextConfiguration and @TestConfiguration.

#### Log4j2 integration to replace Logback
By default Spring uses Logback which is included in the Spring starters. In this project it has been excluded from the dependencies and Log4j2 is used instead and configured using a log4j2-spring.xml file. Also configured is a custom appender and logger pair that outputs in JSON triggered via a REST controller endpoint.
