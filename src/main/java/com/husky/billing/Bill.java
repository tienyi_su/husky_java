package com.husky.billing;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author Tien-Yi Su
 */
@Entity
@Table(name = "billing_table")
@Getter
@Setter
@Builder
public class Bill
{
    @Id
    @Min(1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long billNumber;

    private String customerName;

    private BigDecimal total;
}
