package com.husky.billing;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Tien-Yi Su
 */
@RestController
@RequestMapping("/billing")
public class BillingController
{
    private BillingService service;

    public BillingController(final BillingService pService)
    {
        service = pService;
    }

    @GetMapping
    public Bill createInvoice(@RequestParam(value = "name") final String pName)
    {
        return service.create(pName);
    }
}
