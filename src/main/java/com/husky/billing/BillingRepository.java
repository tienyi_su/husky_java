package com.husky.billing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Tien-Yi Su
 */
@Repository
public interface BillingRepository
    extends JpaRepository<Bill, Long>
{
}
