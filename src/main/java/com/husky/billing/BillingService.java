package com.husky.billing;

import com.husky.order.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Tien-Yi Su
 */
@Service
public class BillingService
{
    private OrderService service;

    private BillingRepository repository;

    public BillingService(final OrderService pService, final BillingRepository pRepository)
    {
        service = pService;
        repository = pRepository;
    }

    public Bill create(final String pCustomerName)
    {
        Bill bill = Bill.builder().customerName(pCustomerName).total(service.sum(pCustomerName)).build();

        repository.save(bill);

        return bill;
    }
}
