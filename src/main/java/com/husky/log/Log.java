package com.husky.log;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Tien-Yi Su
 */
@Getter
@Setter
public class Log
{
    private String message;
}
