package com.husky.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Tien-Yi Su
 */
@RestController
@RequestMapping("/log")
public class LogController
{
    private Logger LOGGER = LogManager.getLogger("JSONLogger");

    @PostMapping
    public HttpStatus createLog(@RequestBody final Log pLog)
    {
        LOGGER.info(pLog.getMessage());

        return HttpStatus.CREATED;
    }
}