package com.husky.order;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * @author Tien-Yi Su
 */
@Entity
@Table(name = "order_table")
@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class Order
{
    @Id
    @Min(1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderNumber;

    private String customerName;

    private String itemName;

    private BigDecimal total;
}
