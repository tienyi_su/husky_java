package com.husky.order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Tien-Yi Su
 */
public interface OrderRepository
    extends JpaRepository<Order, Long>
{
    public List<Order> findAllByCustomerName(String pCustomerName);
}
