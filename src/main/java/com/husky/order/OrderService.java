package com.husky.order;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Tien-Yi Su
 */
@Service
public class OrderService
{
    private OrderRepository repository;

    public OrderService(final OrderRepository pRepository)
    {
        repository = pRepository;
    }

    public BigDecimal sum(final String pCustomerName)
    {
        List<Order> orders = repository.findAllByCustomerName(pCustomerName);

        return orders.stream().map(Order::getTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
