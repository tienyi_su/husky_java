package com.husky.billing;

import com.husky.order.Order;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import static java.math.BigDecimal.valueOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * @author Tien-Yi Su
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@AutoConfigureTestEntityManager
@Transactional
public class BillingControllerIntegrationTest
{
    @Autowired
    private TestEntityManager em;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup()
        throws Exception
    {
        mockMvc = webAppContextSetup(webApplicationContext).build();

        em.persist(new Order().setCustomerName("Jane Appleseed").setItemName("X-Wing").setTotal(valueOf(200.5)));
        em.persist(new Order().setCustomerName("Jane Appleseed").setItemName("TIE Fighter").setTotal(valueOf(150.0)));
        em.flush();
    }

    @Test
    public void createInvoice_returnInvoice()
        throws Exception
    {
        mockMvc.perform(get("/billing").param("name", "Jane Appleseed"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("billNumber").value(1))
            .andExpect(jsonPath("customerName").value("Jane Appleseed"))
            .andExpect(jsonPath("total").value(350.5));
    }
}
