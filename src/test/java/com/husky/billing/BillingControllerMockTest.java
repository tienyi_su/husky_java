package com.husky.billing;

import com.husky.HuskyApplication;
import com.husky.order.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import static java.math.BigDecimal.valueOf;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * @author Tien-Yi Su
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HuskyApplication.class)
@WebAppConfiguration
public class BillingControllerMockTest
{
    @MockBean
    private OrderService orderService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup()
        throws Exception
    {
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createInvoice_returnInvoice()
        throws Exception
    {
        when(orderService.sum("Jane Appleseed")).thenReturn(valueOf(350.5));

        mockMvc.perform(get("/billing").param("name", "Jane Appleseed"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("billNumber").value(1))
            .andExpect(jsonPath("customerName").value("Jane Appleseed"))
            .andExpect(jsonPath("total").value(350.5));
    }
}
