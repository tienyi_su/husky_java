package com.husky.billing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import static java.math.BigDecimal.valueOf;
import static org.junit.Assert.assertNotNull;


/**
 * @author Tien-Yi Su
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BillingRepositoryIntegrationTest
{
    @Autowired
    private BillingRepository repository;

    @Test
    public void save_validBill_verifyExistsInRepository()
    {
        Bill bill = Bill.builder().customerName("Jane Appleseed").total(valueOf(100.0)).build();

        repository.save(bill);

        assertNotNull(repository.findById(1L));
    }
}
