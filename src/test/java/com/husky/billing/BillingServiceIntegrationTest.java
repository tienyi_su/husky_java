package com.husky.billing;

import com.husky.billing.Bill;
import com.husky.billing.BillingService;
import com.husky.order.Order;
import com.husky.order.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author Tien-Yi Su
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestEntityManager
@Transactional
public class BillingServiceIntegrationTest
{
    @Autowired
    private TestEntityManager em;

    @Autowired
    private BillingService billingService;

    @Test
    public void create_saveAndReturnBill()
    {
        em.persist(new Order().setCustomerName("Jane Appleseed").setItemName("X-Wing").setTotal(valueOf(50.0)));
        em.persist(new Order().setCustomerName("Jane Appleseed").setItemName("TIE Fighter").setTotal(valueOf(50.0)));
        em.flush();

        Bill bill = billingService.create("Jane Appleseed");

        assertThat(bill.getCustomerName(), is("Jane Appleseed"));
        assertThat(bill.getTotal(), is(valueOf(100.0)));
    }
}
