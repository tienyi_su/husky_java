package com.husky.billing;

import com.husky.billing.Bill;
import com.husky.billing.BillingRepository;
import com.husky.billing.BillingService;
import com.husky.order.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


/**
 * @author Tien-Yi Su
 */
@RunWith(MockitoJUnitRunner.class)
public class BillingServiceMockitoTest
{
    @Mock
    private OrderService orderService;

    @Mock
    private BillingRepository repository;

    @InjectMocks
    private BillingService billingService;

    @Before
    public void before()
    {
        initMocks(this);
    }

    @Test
    public void create_validCustomer_saveAndReturnBill()
    {
        when(orderService.sum("Jane Appleseed")).thenReturn(valueOf(100.0));

        Bill bill = billingService.create("Jane Appleseed");

        verify(repository).save(bill);

        assertThat(bill.getCustomerName(), is("Jane Appleseed"));
        assertThat(bill.getTotal(), is(valueOf(100.0)));
    }
}
