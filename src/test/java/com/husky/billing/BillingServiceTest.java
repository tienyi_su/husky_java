package com.husky.billing;

import com.husky.billing.Bill;
import com.husky.billing.BillingService;
import com.husky.order.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


/**
 * @author Tien-Yi Su
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BillingServiceTest
{
    @MockBean
    private OrderService orderService;

    @Autowired
    private BillingService billingService;

    @Test
    public void create_validCustomer_saveAndReturnBill()
    {
        when(orderService.sum("Jane Appleseed")).thenReturn(valueOf(100.0));

        Bill bill = billingService.create("Jane Appleseed");

        assertThat(bill.getCustomerName(), is("Jane Appleseed"));
        assertThat(bill.getTotal(), is(valueOf(100.0)));
    }
}
